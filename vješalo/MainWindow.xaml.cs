﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace vješalo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int broj_pokusaja = 4;
        int broj_slova = 0;
       
        char slovo = 'A';
        string recenica = "to je to", pogadjaj = "";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Unesi_slovo_Click(object sender, RoutedEventArgs e)
        {
            
            char.TryParse(Slovo.Text, out slovo);
            if (broj_pokusaja > 0)
            {
                char[] ch = pogadjaj.ToCharArray();
                pogadjaj = "";
                for (int i = 0; i < broj_slova+1 ; i++)
                {
                    if (recenica[i] == slovo)
                    {

                        ch[i] = slovo;
                    }
                    pogadjaj += ch[i];
                }

                Recenica.Content = pogadjaj;
                Unesena_slova.Content += slovo.ToString();
                broj_pokusaja--;
            }
            if (broj_pokusaja == 0 && pogadjaj != recenica)
            {
                MessageBox.Show("IZGUBILI STE", "KRAJ!");
            }
            if (pogadjaj == recenica)
            {
                MessageBox.Show("POBEDILI STE", "KRAJ!");
            }
            pokusaji.Content = broj_pokusaja;

        }
      
        private void Load(object sender, EventArgs e)
        {
            pogadjaj = "";
            reset();

        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            reset();
        }

        void reset()
        {
            Unesena_slova.Content += "Unesena_slova :";
            pokusaji.Content = broj_pokusaja;
            List<string> linije = new List<string>();
            
            using (var reader = new StreamReader("Pitanja.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    linije.Add(line);
                }
            }

            Random rnd = new Random();
            int r;
            r = rnd.Next(0, 1);
           
            recenica = linije[r];
            broj_slova = linije[r].Length-1;
            pogadjaj = "";
            for (int i = 0; i < 8; i++)
            {
                if (recenica[i] != ' ')
                {
                    pogadjaj += "X";
                }
                else pogadjaj += " ";
            }
            Recenica.Content = pogadjaj;
        }
    }
}
